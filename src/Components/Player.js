import React from "react";

import Button from "./Button";

import { Card, PlayerHand } from "../Styles/Styled";

let inputItem = '';
let newName = ''

function handleChange(e, change) {
	newName = e.target.value;
	change(e.target.value)
}

function Player({ editFlag, setFlag, name, editPlayerName, hands, removePlayer }) {

	return (

		<article>

			{
				editFlag ? <input onChange={(e) => handleChange(e, editPlayerName)} type="text" ref={(input) => { inputItem = input; }} /> : name = newName
			}

			<Button action={setFlag} icon="✏️">Edit</Button>
			<Button action={removePlayer} icon="🔥">Remove</Button>
			
			<PlayerHand>
				{hands && hands.map((hand, i) =>
					<Card key={i} suit={Array.from(hand[1]).toString()} value={Array.from(hand[0]).toString()} selected={true}>
						{Array.from(hand[0]).toString()}
					</Card>
				)}
			</PlayerHand>
		</article>
	)
}



export default Player;
