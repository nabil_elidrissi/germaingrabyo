import React, { Component } from 'react';
import poker from 'pokersolver';
import { suits, values } from "../utils";

import Layout from "./Layout";
import Deck from "./Deck";
import Player from "./Player";
import Button from "./Button";

import { Footer } from "../Styles/Styled";

class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
			hands : [], 
			players:[],
			player:{}, 
			playerName:'', 
			editFlag:false, 
			winnner:null
		}
	}

	findWinner(hands) {		
		var finalHands = [];

		var result = [];

		switch (hands.length) {
			case 2:
				var hand1 = poker.Hand.solve(hands[0].hands);
				var hand2 = poker.Hand.solve(hands[1].hands);
				finalHands.push(hand1, hand2);
				result = poker.Hand.winners(finalHands)
				break;
			case 3:
				var hand1 = poker.Hand.solve(hands[0].hands);
				var hand2 = poker.Hand.solve(hands[1].hands);
				var hand3 = poker.Hand.solve(hands[2].hands);
				finalHands.push(hand1, hand2, hand3);
				result = poker.Hand.winners(finalHands)
				break;
			case 4:
				var hand1 = poker.Hand.solve(hands[0].hands);
				var hand2 = poker.Hand.solve(hands[1].hands);
				var hand3 = poker.Hand.solve(hands[2].hands);
				var hand4 = poker.Hand.solve(hands[3].hands);
				finalHands.push(hand1, hand2, hand3, hand4);
				result = poker.Hand.winners(finalHands)
				break;
			case 5:
				var hand1 = poker.Hand.solve(hands[0].hands);
				var hand2 = poker.Hand.solve(hands[1].hands);
				var hand3 = poker.Hand.solve(hands[2].hands);
				var hand4 = poker.Hand.solve(hands[3].hands);
				var hand5 = poker.Hand.solve(hands[4].hands);
				finalHands.push(hand1, hand2, hand3, hand4, hand5);
				result = poker.Hand.winners(finalHands)
				break;
			case 6:
				var hand1 = poker.Hand.solve(hands[0].hands);
				var hand2 = poker.Hand.solve(hands[1].hands);
				var hand3 = poker.Hand.solve(hands[2].hands);
				var hand4 = poker.Hand.solve(hands[3].hands);
				var hand5 = poker.Hand.solve(hands[4].hands);
				var hand6 = poker.Hand.solve(hands[5].hands);
				finalHands.push(hand1, hand2, hand3, hand4, hand5, hand6);
				result = poker.Hand.winners(finalHands)
				break;
			default:
		}

			this.setState((prevState) => {
				return { winner: prevState.winner = result[0].descr }
		  	})
	}

	setFlag = () => {
		this.setState((prevState) => {
			return { editFlag: prevState.editFlag = !prevState.editFlag }
		  })
	}

	editPlayerName = (name) => {
		this.setState((prevState) => {
     			return { playerName: prevState.playerName = name }
		  })
	}

	addPlayer = (player) => {
		player = [this.state.player];
		player.hands = this.addHand(suits, values);
		player.name = this.state.playerName

		const players = [...this.state.players];
		// limit players
		if(players.length <= 5) {
			players.push(player);
		}
		 this.setState((prevState) => {
     		return { players: prevState.players = players }
		   })
	   }
	   
	   removePlayer = (playerToRemove) => {
			const players = [...this.state.players];
			players.splice(playerToRemove, 1);
			this.setState((prevState) => {
     			return { players: prevState.players = players }
  			})
	   }

	addHand = (suits, values) => {

		let hands = [...this.state.hands];
		for(let i = 0; i < values.length; i++) {
			for(let j = 0; j < suits.length; j++) {
				hands.push(values[i].concat(suits[j]));
			}
		}
		var singleHand = Array.from(this.createHand(hands));		
		return singleHand;
		
	}

	createHand = (deck) => {
		let hand = new Set();
		for(let i = 0; i < 5; i++) {
			hand.add(deck[Math.max(Math.floor(Math.random() * 52)) ])
		}
		return hand;
	}

	render() {

		return (
				<Layout>
					<section>
						<h1>Cards deck</h1>
						<Deck suits={suits} values={values} />
					</section>
					<section>
						<header>
							<h1>Players</h1>
						</header>
						<section>
						     {this.state && this.state.players && this.state.players.map((player, i) =>
								 <Player findWinner={this.findWinner.bind(this)} setFlag={this.setFlag.bind(this)} editFlag={this.state.editFlag} editPlayerName={this.editPlayerName.bind(this)} removePlayer={this.removePlayer.bind(this, i)} key={i} hands={player.hands} name={this.state.playerName} />
             				 )} 
						</section>
						<Footer>
						<Button action={this.addPlayer.bind(this, this.state.player)} icon="🙋‍♀️">Add new player</Button>
							<Button action={this.findWinner.bind(this,this.state.players)} icon="🏆">Find the winner</Button>
						</Footer>
					</section>
					<p> {this.state.winner ? `The winning hand was a ${this.state.winner}` : ''} </p>
				</Layout>
		);
	}
}

export default App;
